﻿using Automation.Web.Common.PageObjects;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Automation.Web.Tests.Steps.Wishlist
{
    [Binding]
    public class WishlistAdd
    {

        private ProductPage _productPage;
        private HomePage homePage;
        private readonly string product = "//*[@class = 'Product-name js-ProductClickListener' and contains(text(), 'Trotineta electrica')]";

        public WishlistAdd(ProductPage productPage, HomePage home)
        {
            this._productPage = productPage;
            this.homePage = home;
        }
        [Given(@"I click on autetificare button")]
        public void GivenIClickOnAutetificareButton()
        {
            this.homePage.ClickAutentificare();
        }

        [When(@"I press the wishlist button")]
        public void WhenIPressTheWishlistButton()
        {
            this._productPage.clickOnProduct("//a[@class='Product-name ' and contains(text(), 'Trotineta electrica XIAOMI MI PRO 2')]");
            this._productPage.addToWishlist();
        }

        [When(@"I navigate to wislist page")]
        public void WhenINavigateToWislistPage()
        {
            this.homePage.ClickContulMeu();
            this.homePage.ClickWishListTab();
        }

        [Then(@"The product should be displayed into wishlist")]
        public void ThenTheProductShouldBeDisplayedIntoWishlist()
        {
            Assert.True(this._productPage.checkProduct("trotineta electrica xiaomi mi pro 2", product));
        }

    }
}
