﻿namespace Automation.Web.Tests.Steps.Product
{
    using Automation.Web.Common.PageObjects;
    using NUnit.Framework;
    using TechTalk.SpecFlow;

    [Binding]
    public class SearchProduct
    {
        private readonly HomePage _homePage;
        private readonly ProductPage _productPage;

        public SearchProduct(HomePage homePage, ProductPage productPage)
        {
            this._homePage = homePage;
            this._productPage = productPage;
        }

        [Given(@"I go to home page")]
        public void GivenIGoToHomePage()
        {
            _homePage.NavigateToHomePage();
        }

        [Given(@"I'm looking for ""(.*)""")]
        public void WhenIMLookingFor(string product)
        {
            _homePage.SearchByName(product);
        }

        [When(@"I press the cauta button")]
        public void WhenIPressTheCautaButton()
        {
            _homePage.ClickCauta();
        }

        [Then(@"I should find the desired product on the page")]
        public void ThenIShouldFindTheDesiredProductOnThePage()
        {
            Assert.True(_productPage.checkProduct("trotineta electrica xiaomi mi pro 2", "//a[@class ='Product-name ']"));
        }
    }
}
