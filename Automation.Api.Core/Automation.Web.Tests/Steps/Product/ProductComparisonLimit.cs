﻿using Automation.Web.Common.PageObjects;
using NUnit.Framework;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Automation.Web.Tests.Steps.Product
{
    [Binding]
    public class ProductComparisonLimit
    {
        private readonly HomePage _homePage;
        private readonly ProductPage _productPage;


        public ProductComparisonLimit(HomePage homePage, ProductPage productPage)
        {
            this._homePage = homePage;
            this._productPage = productPage;
        }

        [When(@"I'm comparing the following products")]
        public void GivenIMComparingTheFollowingProducts(Table table)
        {
            var productList = table.CreateSet<SearchProductInTable>();
            foreach (var line in productList)
            {
                Thread.Sleep(2000);
                this._homePage.SearchByName(line.product.ToString());
                this._homePage.ClickCauta();
                Thread.Sleep(2000);
                this._productPage.addProductToComparison();

            }

        }
        [Then(@"An error message should appear")]
        public void ThenAErrorMessageShouldAppear()
        {
            Assert.True(this._productPage.checkIfErrorMessageIsDisplayed());
        }

    }
    public class SearchProductInTable
    {
        public string product { get; set; }
    }
}
