﻿namespace Automation.Web.Tests.Steps.Chart
{
    using Automation.Web.Common.PageObjects;
    using NUnit.Framework;
    using System.Threading;
    using TechTalk.SpecFlow;

    [Binding]
    public class ChartSteps
    {
        private readonly ProductPage _productPage;
        private readonly HomePage _homePage;

        public ChartSteps(ProductPage product, HomePage homePage)
        {
            this._productPage = product;
            this._homePage = homePage;
        }

        [When(@"I press the adaugaInCos button")]
        public void WhenIPressTheAdaugaInCosButton()
        {
            _productPage.acceptCookie();
            _productPage.addProductToChart();
        }

        [When(@"I close the popup page")]
        public void WhenICloseThePopupPage()
        {
            Thread.Sleep(3000);
            _productPage.closePopUp();
        }

        [Then(@"The product should be displayed into chart")]
        public void ThenTheProductShouldBeDisplayedIntoChart()
        {
            _homePage.ClickOnCart();
            Assert.True(_productPage.checkProduct("trotineta electrica xiaomi mi pro 2", "//a[@class = 'Media-link']"));
        }

    }
}
