﻿using Automation.Web.Common.PageObjects;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Automation.Web.Tests.Steps.Chart
{
    [Binding]
    public class EmptyCart
    {

        private readonly CartPage _cartPage;

        public EmptyCart(CartPage cart)
        {
            this._cartPage = cart;
        }

        [When(@"I open the cart")]
        public void WhenIOpenTheCart()
        {
            _cartPage.goToCart();
        }

        [When(@"I press the goleste cosul button")]
        public void WhenIPressTheGolesteCosulButton()
        {
            _cartPage.EmptyCart();
        }

        [Then(@"The cart should be empty")]
        public void ThenTheCartShouldBeEmpty()
        {
            Assert.True(_cartPage.CheckProduct());
        }

    }
}