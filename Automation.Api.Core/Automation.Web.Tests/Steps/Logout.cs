﻿namespace Automation.Web.Tests.Steps
{
    using Automation.Web.Common.PageObjects;
    using NUnit.Framework;
    using System.Threading;
    using TechTalk.SpecFlow;

    [Binding]
    public class Logout
    {
        private readonly HomePage _homePage;

        public Logout(HomePage homePage)
        {
            this._homePage = homePage;
        }

        [When(@"I'm logged in successfully")]
        public void WhenIMLoggedInSuccessfully()
        {
            Thread.Sleep(4000);
            _homePage.CheckUserName("test");
        }

        [When(@"I press the logout button")]
        public void WhenIPressTheLogoutButton()
        {
            _homePage.Logout();
        }

        [Then(@"I should be logged out")]
        public void ThenIShouldBeLoggedOut()
        {
            Assert.True(_homePage.CheckUserName("Contul meu"));
        }

    }
}
