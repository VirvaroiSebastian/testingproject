﻿namespace Automation.Web.Tests.Steps.Register
{
    using Automation.Web.Common.PageObjects;
    using NUnit.Framework;
    using TechTalk.SpecFlow;
    using TechTalk.SpecFlow.Assist;

    [Binding]
    public class RegisterSuccessfully
    {
        private readonly RegisterPage _registerPage;
        private readonly RegisterBuilder _builder;
        private RegisterDetails _registerDetails;
        private readonly HomePage _homePage;
        private readonly ProductPage _productPage;

        public RegisterSuccessfully(RegisterPage register, RegisterBuilder builder, HomePage homePage,
            ProductPage productPage)
        {
            this._registerPage = register;
            this._builder = builder;
            this._homePage = homePage;
            this._productPage = productPage;
        }

        [Given(@"I accept the cookies")]
        public void GivenIAcceptTheCookies()
        {
            _productPage.acceptCookie();
        }

        [Given(@"I navigate to register page")]
        public void GivenINavigateToRegisterPage()
        {
            _registerPage.NavigateToRegisterPage();
        }

        [Given(@"I complete all the fields")]
        public void GivenICompleteAllTheFields(Table registerTable)
        {
            System.Random random = new System.Random();
            this._registerDetails = registerTable.CreateInstance<RegisterDetails>();
            this._builder
                .WithFirstName(this._registerDetails.prenume)
                .WithLastName(this._registerDetails.nume)
                .WithEmail(this._registerDetails.email + random.Next(50) + "@yahoo.com ")
                .WithTelephone(this._registerDetails.numarDeTelefon + random.Next(10,99))
                .WithPassword(this._registerDetails.parola)
                .WithPasswordConfirmation(this._registerDetails.confirmaParola);
        }

        [Given(@"I accept licence agreement and newletter")]
        public void GivenIAcceptLicenceAgreementAndNewletter()
        {
            _registerPage.CheckLicenseAgreement();
            _registerPage.CheckNewsletter();
        }

        [When(@"I press the register button")]
        public void WhenIPressTheRegisterButton()
        {
            _registerPage.ClickOnRegisterButton();
        }

        [Then(@"I should be register successfully")]
        public void ThenIShouldBeRegisterSuccessfully()
        {
            Assert.True(_homePage.CheckUserName("test"));
        }

        public class RegisterDetails
        {
            public string prenume { get; set; }
            public string nume { get; set; }
            public string email { get; set; }
            public string numarDeTelefon { get; set; }
            public string parola { get; set; }
            public string confirmaParola { get; set; }

        }


    }
}

