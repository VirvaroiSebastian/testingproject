﻿using Automation.Web.Common.PageObjects;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using static Automation.Web.Tests.Steps.Register.RegisterSuccessfully;

namespace Automation.Web.Tests.Steps.Register
{
    [Binding]
    public class InvalidPhoneNumberRegister
    {
        private readonly RegisterBuilder _builder;
        private RegisterDetails _registerDetails;
        private RegisterPage _registerPage;
        private readonly string _invalidPhoneNumberErrorMessage = "//div[@class = 'Form-validate is-active']";

        public InvalidPhoneNumberRegister(RegisterBuilder builder, RegisterPage registerPage)
        {
            this._builder = builder;
            this._registerPage = registerPage;
        }

        [When(@"I complete all the fields")]
        public void WhenICompleteAllTheFields(Table registerTable)
        {
            this._registerDetails = registerTable.CreateInstance<RegisterDetails>();
            this._builder
                .WithFirstName(this._registerDetails.prenume)
                .WithLastName(this._registerDetails.nume)
                .WithEmail(this._registerDetails.email)
                .WithTelephone(this._registerDetails.numarDeTelefon)
                .WithPassword(this._registerDetails.parola)
                .WithPasswordConfirmation(this._registerDetails.confirmaParola);
        }

        [Then(@"I should get an error message with invalid phone number format")]
        public void ThenIShouldGetAnErrorMessageWithInvalidPhoneNumberFormat()
        {
            Assert.True(this._registerPage.CheckIfTextIsPresentOnPage(_invalidPhoneNumberErrorMessage, "Numar de telefon invalid"));
        }


    }
}
