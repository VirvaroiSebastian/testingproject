﻿namespace Automation.Web.Tests.Steps
{
    using Automation.Web.Common.PageObjects;
    using NUnit.Framework;
    using System.Threading;
    using TechTalk.SpecFlow;

    [Binding]
    public class Login
    {
        public HomePage _homePage;

        public Login(HomePage homePage)
        {
            this._homePage = homePage;
        }

        [Given(@"I open the altex website")]
        public void GivenIOpenTheAltexWebsite()
        {
            _homePage.NavigateToHomePage();
        }

        [Given(@"I click on contul meu button")]
        public void GivenIClickOnContulMeuButton()
        {
            _homePage.ClickContulMeu();
        }

        [Given(@"I insert the correct credentials")]
        public void GivenIInsertTheCorrectCredentials()
        {
            _homePage.LoginWithValidCredentials();
        }

        [When(@"I press the autentificare button")]
        public void WhenIPressTheAutentificareButton()
        {
            _homePage.ClickAutentificare();
        }

        [Then(@"I should be redirected to a home page")]
        public void ThenIShouldBeRedirectedToAHomePage()
        {
            Thread.Sleep(10000);
            Assert.True(_homePage.CheckUserName("test"));
        }

    }
}
