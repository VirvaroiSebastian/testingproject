﻿Feature: InvalidPhoneNumberRegister

Scenario: Register Successfully
	Given I navigate to register page
	And I accept the cookies
	When I complete all the fields
		| Prenume | Nume | Email           | Numar de telefon | Parola    | Confirma parola |
		| test    | test | test3@yahoo.com | 075767898712412  | testulmeu | testulmeu       |
	Then  I should get an error message with invalid phone number format