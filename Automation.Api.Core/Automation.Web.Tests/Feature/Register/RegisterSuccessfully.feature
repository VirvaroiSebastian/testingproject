﻿Feature: RegisterSuccessfully

Scenario: Register Successfully
	Given I navigate to register page
	And I accept the cookies
	And I complete all the fields
		| Prenume | Nume | Email | Numar de telefon | Parola    | Confirma parola |
		| test    | test | test3 | 07576789         | testulmeu | testulmeu       |
	And I accept licence agreement and newletter
	When I press the register button
	Then I should be register successfully