﻿Feature: Logout
		As a user i want to be able to log out

Scenario: Logout successfully
Given I open the altex website
And I accept the cookies
And  I click on contul meu button
And I insert the correct credentials
When I press the autentificare button
And I'm logged in successfully
And I press the logout button 
Then I should be logged out