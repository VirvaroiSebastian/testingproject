﻿Feature: SearchProduct
		 As a user i want to be able to search for the desired product

Scenario: Searching for a product which is in database
Given I go to home page
And I'm looking for "trotineta electrica xiaomi mi pro 2"
When I press the cauta button
Then I should find the desired product on the page
