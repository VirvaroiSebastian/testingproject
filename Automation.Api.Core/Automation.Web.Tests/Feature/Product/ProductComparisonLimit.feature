﻿Feature: ProductComparisonLimit

Scenario: Check if a pop up message will be shown when user add more than 4 products in comparison feature
	Given I open the altex website
	And I accept the cookies
	When I'm comparing the following products
		| Product                               |
		| Televizor QLED Smart SAMSUNG 75Q800T  |
		| Televizor LED Smart LG 43UN73003LC    |
		| Televizor LED Smart SONY 65XH9096B    |
		| Televizor LED Smart VORTEX V75R0112S  |
		| Televizor LED Smart PHILIPS 65PUS7906 |
	Then An error message should appear