﻿Feature: Wishlist

Scenario: Adding a product to the wishlist
Given I open the altex website
And I accept the cookies
And I click on contul meu button
And I insert the correct credentials
And I click on autetificare button
And I'm looking for "trotineta electrica xiaomi mi pro 2"
When I press the cauta button
And I press the wishlist button 
And I navigate to wislist page
Then The product should be displayed into wishlist