﻿namespace Automation.Web.Tests
{
    using Autofac;
    using Common.PageObjects;
    using Core;
    using OpenQA.Selenium;
    public class DataModule : Module
    {
        protected override void Load(ContainerBuilder containerBuilder)
        {
            containerBuilder.Register(c => Driver.LocalDriver()).As<IWebDriver>().SingleInstance();
            containerBuilder.RegisterType<HomePage>().AsSelf();
            containerBuilder.RegisterType<RegisterPage>().AsSelf();
            containerBuilder.RegisterType<ProductPage>().AsSelf();
            containerBuilder.RegisterType<RegisterBuilder>().AsSelf();
            containerBuilder.RegisterType<CartPage>().AsSelf();
        }
    }
}
