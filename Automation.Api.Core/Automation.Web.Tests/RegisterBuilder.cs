﻿using Automation.Web.Common.PageObjects;
using System.Threading;

namespace Automation.Web.Tests
{
    public class RegisterBuilder
    {
        private readonly RegisterPage _registerPage;
        public RegisterBuilder(RegisterPage registerPage)
        {
            this._registerPage = registerPage;
        }

        public RegisterBuilder Build()
        {
            this._registerPage.EnterFirstName("test");
            this._registerPage.EnterLastName("test");
            this._registerPage.EnterEmail("test2@yahoo.com");
            this._registerPage.EnterTelephone("0757678763");
            this._registerPage.EnterPassword("test1234");
            this._registerPage.EnterPasswordConfirmation("test1234");
            this._registerPage.CheckLicenseAgreement();
            this._registerPage.CheckNewsletter();
            return this;
        }

        public RegisterBuilder WithFirstName(string firstName)
        {
            this._registerPage.EnterFirstName(firstName);
            return this;
        }
        public RegisterBuilder WithLastName(string lastName)
        {
            this._registerPage.EnterLastName(lastName);
            return this;
        }
        public RegisterBuilder WithEmail(string email)
        {
            this._registerPage.EnterEmail(email);
            return this;
        }
        public RegisterBuilder WithTelephone(string telephone)
        {
            
            this._registerPage.EnterTelephone(telephone);
            return this;
        }
        public RegisterBuilder WithPassword(string password)
        {
            Thread.Sleep(2000);
            this._registerPage.EnterPassword(password);
            return this;
        }
        public RegisterBuilder WithPasswordConfirmation(string pass)
        {
            this._registerPage.EnterPasswordConfirmation(pass);
            return this;
        }
        public RegisterBuilder WithLicenceAgreement()
        {
            this._registerPage.CheckLicenseAgreement();
            return this;
        }
        public RegisterBuilder WithNewsletter()
        {
            this._registerPage.CheckNewsletter();
            return this;
        }
    }
}
