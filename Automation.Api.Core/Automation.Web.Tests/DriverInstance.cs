﻿namespace Automation.Web.Tests
{
    using NUnit.Framework;
    using OpenQA.Selenium;
    using TechTalk.SpecFlow;

    [Binding]
    public class DriverInstance
    {
        private readonly IWebDriver _driver;

        public DriverInstance(IWebDriver driver)
        {
            this._driver = driver;
        }

        [AfterScenario(Order = 0)]
        public void AfterScenario()
        {
            _driver?.Dispose();
        }

        [TearDown]
        public void TearDown()
        {
            this._driver.Close();
            this._driver.Quit();
        }
    }
}
