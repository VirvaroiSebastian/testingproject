﻿namespace Automation.Web.Tests
{
    using Autofac;
    using SpecFlow.Autofac;
    using System;
    using System.Linq;
    using TechTalk.SpecFlow;
    using ContainerBuilder = Autofac.ContainerBuilder;

    [Binding]
    public class DependencyContainer
    {
        [ScenarioDependencies]
        public static ContainerBuilder CreateContainerBuilder()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule<DataModule>();

            containerBuilder.RegisterTypes(
                typeof(DataModule).Assembly.GetTypes().Where(t => Attribute.IsDefined(t, typeof(BindingAttribute)))
                    .ToArray()).SingleInstance();

            return containerBuilder;
        }
    }
}
