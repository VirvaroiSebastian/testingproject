﻿namespace Automation.Web.Core
{
    using OpenQA.Selenium;
    public class Browser
    {
        public readonly IWebDriver Driver;

        protected Browser (IWebDriver driver)
        {
            this.Driver = driver;
        }

    }
}
