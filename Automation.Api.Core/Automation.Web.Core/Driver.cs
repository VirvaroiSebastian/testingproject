﻿namespace Automation.Web.Core
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using System;

    public class Driver
    {
        public static IWebDriver LocalDriver()
        {
            var chromeDriver = new ChromeDriver();

            chromeDriver.Manage().Window.Maximize();
            chromeDriver.Manage().Cookies.DeleteAllCookies();
            chromeDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            return chromeDriver;
        }
    }
}
