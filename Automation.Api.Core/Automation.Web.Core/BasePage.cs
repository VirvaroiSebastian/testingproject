﻿using OpenQA.Selenium;

namespace Automation.Web.Core
{
    public class BasePage : Browser
    {
        public BasePage(IWebDriver driver) : base(driver)
        { }
    }
}
