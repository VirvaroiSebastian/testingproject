﻿namespace Automation.Web.Common
{
    using Automation.Web.Core;
    using OpenQA.Selenium;

    public class Page : BasePage
    {
        protected Page(IWebDriver driver) : base(driver) { }
    }
}
