﻿namespace Automation.Web.Common.Utils
{
    using OpenQA.Selenium;
    public static class UtilsFunctions
    {
        public static string Split(IWebDriver driver, string xpathSelector)
        {
            string sir = driver.FindElement(By.XPath($"{xpathSelector}")).Text;
            string[] splits = sir.Split(',');
            var name = splits[0].ToString();
            return name;
        }
    }
}
