﻿using System;

namespace Automation.Web.Common
{
    public static class UrlBuilder
    {
        private static string baseUrl = "https://mediagalaxy.ro/home/";

        public static string LoginUri()
        {
            return CreateURL(baseUrl, string.Empty);
        }

        public static string RegisterUri()
        {
            return CreateURL(baseUrl, RelativeUri.register);
        }

        public static string CreateURL(string baseUrl, string relativeUri)
        {
            var baseUri = new UriBuilder(baseUrl);

            if (Uri.TryCreate(baseUri.Uri, relativeUri, out var newUri))
                return newUri.ToString();
            throw new ArgumentException("Can't create this url."); 
        }
    }
}
