﻿namespace Automation.Web.Common.PageObjects
{
    using OpenQA.Selenium;
    using System.Threading;

    public class ProductPage : Page
    {
        public ProductPage(IWebDriver driver) : base(driver)
        { }

        private readonly By _adauga = By.XPath("//div[text() = 'Adauga in cos']");
        private readonly By _closePopUp = By.XPath("//i[@class='close icon']");
        private readonly By _acceptCookie = By.XPath("//button[text() = 'Accepta']");
        private readonly By _wishlist = By.XPath("//*[@class='flex items-center']//span[text() = 'Wishlist']");
        private readonly By _compareButton = By.XPath("//*[@class='flex items-center']//span[text() = 'Compara']");
        private readonly string compareButtonList = "compare-container";
        private readonly By _errorCompareListMessage = By.XPath("//div[@class = 'flex-grow' and contains(text() , 'Nu poti adauga mai mult de 4 produse in lista de comparatie.')]");


        public bool checkProduct(string productName, string xpathSelector)
        {
            var name = Utils.UtilsFunctions.Split(this.Driver, xpathSelector);
            try
            {
                if (string.Equals(name.ToUpper(), productName.ToUpper()) == true)
                {
                    return true;
                }
            }
            catch (NoSuchElementException ex)
            {
                System.Console.WriteLine("Exception was thrown");
            }
            return false;
        }

        public void addToWishlist()
        {
            Thread.Sleep(2000);
            this.Driver.FindElement(_wishlist).Click();
        }
        public void acceptCookie()
        {
            this.Driver.FindElement(_acceptCookie).Click();
        }

        public void closePopUp()
        {
            this.Driver.FindElement(_closePopUp).Click();
        }

        public void addProductToChart()
        {
            this.Driver.FindElement(_adauga).Click();
        }
        public void addProductToComparison()
        {
            this.Driver.FindElement(_compareButton).Click();
        }

        public void clickOnProduct(string xpath)
        {
            this.Driver.FindElement(By.XPath(xpath)).Click();
        }

        public void clickOnCompareList()
        {
            this.Driver.FindElement(By.Id(compareButtonList)).Click();
        }
        public bool checkIfErrorMessageIsDisplayed()
        {
            if (this.Driver.FindElement(_errorCompareListMessage).Displayed == true)
                return true;
            return false;
        }
    }
}
