﻿using OpenQA.Selenium;

namespace Automation.Web.Common.PageObjects
{
    public class CartPage : Page
    {

        public CartPage(IWebDriver driver) : base(driver) { }

        private readonly By _goToCart = By.XPath("//div[@class='border-2 rounded py-1 px-5 border-transparent']//div[text()='Vezi cosul']");
        private readonly By _EmptyCart = By.XPath("//div[text()='Goleste cosul']");
        private readonly By _checkEmpty = By.XPath("//h3[text() = 'Nu ai produse in cos.']");

        public void goToCart()
        {
            this.Driver.FindElement(_goToCart).Click();
        }

        public void EmptyCart()
        {
            this.Driver.FindElement(_EmptyCart).Click();
        }

        public bool CheckProduct()
        {
            bool result = false;
            if (this.Driver.FindElement(_checkEmpty).Text.ToString().Equals("Nu ai produse in cos."))
            {
                result = true;
            }
            return result;
        }

    }

}