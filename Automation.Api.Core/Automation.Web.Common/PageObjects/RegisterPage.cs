﻿namespace Automation.Web.Common.PageObjects
{
    using OpenQA.Selenium;
    using System.Threading;

    public class RegisterPage : Page
    {
        public RegisterPage(IWebDriver driver) : base(driver)
        {
        }

        private readonly By _prenume = By.XPath("//input[@name='first_name']");
        private readonly By _nume = By.XPath("//input[@name='last_name']");
        private readonly By _email = By.CssSelector("input:nth-child(8)");
        private readonly By _telephone = By.XPath("//input[@name='telephone']");
        private readonly By _password = By.CssSelector("input:nth-child(14)");
        private readonly By _passwordConfirm = By.XPath("//input[@name='password_confirm']");
        private readonly By _chkTermeni = By.XPath("//label[@class='Form-checkbox']");
        private readonly By _chkNewsletter = By.CssSelector("div:nth-child(21) > label");
        private readonly By _inregistrare = By.CssSelector(" div.my-4 > button");


        public void ClickOnRegisterButton()
        {
            this.Driver.FindElement(_inregistrare).Click();
        }

        public void NavigateToRegisterPage()
        {
            this.Driver.Navigate().GoToUrl(UrlBuilder.RegisterUri());
        }

        public void EnterFirstName(string firstName)
        {
            this.Driver.FindElement(_prenume).Clear();
            this.Driver.FindElement(_prenume).SendKeys(firstName);
        }
        public void EnterLastName(string lastName)
        {
            this.Driver.FindElement(_nume).Clear();
            this.Driver.FindElement(_nume).SendKeys(lastName);
        }
        public void EnterEmail(string email)
        {
            this.Driver.FindElement(_email).Clear();
            this.Driver.FindElement(_email).SendKeys(email);
        }
        public void EnterTelephone(string telephone)
        {
            this.Driver.FindElement(_telephone).Clear();
            this.Driver.FindElement(_telephone).SendKeys(telephone);
        }
        public void EnterPassword(string password)
        {
            this.Driver.FindElement(_password).Clear();
            this.Driver.FindElement(_password).SendKeys(password);
        }
        public void EnterPasswordConfirmation(string pass)
        {
            this.Driver.FindElement(_passwordConfirm).SendKeys(pass);
        }
        public void CheckLicenseAgreement()
        {
            this.Driver.FindElement(_chkTermeni).Click();
        }
        public void CheckNewsletter()
        {
            this.Driver.FindElement(_chkNewsletter).Click();
        }
        public bool CheckIfTextIsPresentOnPage(string pathToElement, string expectedText)
        {
            string nameOfElement = this.Driver.FindElement(By.XPath(pathToElement)).Text;
            if (nameOfElement.Equals(expectedText))
                return true;
            return false;

        }
    }
}
