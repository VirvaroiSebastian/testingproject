﻿namespace Automation.Web.Common.PageObjects
{
    using OpenQA.Selenium;
    using System.Threading;

    public class HomePage : Page
    {
        public HomePage(IWebDriver driver) : base(driver) { }

        private readonly By _inputCautaProdus = By.XPath("//input[@type = 'text' and @placeholder='Cauta produsul dorit' ]");
        private readonly By _cauta = By.XPath("//div[@class='flex items-center justify-center' and contains(text(), 'Cauta')]");
        private readonly By _contulMeu = By.XPath("//div[@class = 'jsx-2454436330 float-left border-r  text-white']");
        private readonly By _email = By.XPath("//input[@name='email']");
        private readonly By _password = By.XPath("//input[@name='password']");
        private readonly By _autentificare = By.XPath("//div[@class='border-2 rounded py-2px px-4 border-transparent']");
        private readonly By _inregistrare = By.XPath("//div[@class='border-2 rounded py-2px px-4 border-primary group-hover:border-primary-darker']");
        private readonly By _recuperareParola = By.XPath("//a[text()='Recuperare parola']");
        private readonly By _cosullMeu = By.XPath("//div[@class = 'jsx-2454436330 float-left text-white']");
        private readonly By _numeUtilizator = By.XPath("//span[text() = 'Contul meu']");
        private readonly By _iesiDinCont = By.XPath("//a[text() = 'Iesi din cont']");
        private readonly By _wishlistTab = By.XPath("//*[@class = 'jsx-2454436330'] /a[text() = 'Wishlist']");

        public void SearchByName(string product)
        {
            this.Driver.FindElement(_inputCautaProdus).Clear();
            this.Driver.FindElement(_inputCautaProdus).SendKeys(product);
        }

        public void Logout()
        {
            this.Driver.FindElement(_iesiDinCont).Click();
        }

        public void ClickCauta()
        {
            Thread.Sleep(3000);
            this.Driver.FindElement(_cauta).Click();
        }
        public void ClickOnCart()
        {
            this.Driver.FindElement(_cosullMeu).Click();
            this.Driver.FindElement(By.XPath("//div[@class='flex items-center justify-center' and text()='Vezi cosul']")).Click();
        }

        public void NavigateToHomePage()
        {
            this.Driver.Navigate().GoToUrl(UrlBuilder.LoginUri());
        }
        public bool CheckUserName(string containsName)
        {
            Thread.Sleep(15000);
            var user = this.Driver.FindElement(By.XPath($"//span[starts-with(text(), '{containsName}')]")).Text.ToString();
            if (user.Contains(containsName))
                return true;
            return false;
        }
        public void ClickContulMeu()
        {
            this.Driver.FindElement(_contulMeu).Click();
        }
        public void ClickAutentificare()
        {
            this.Driver.FindElement(_autentificare).Click();
        }
        public void ClickWishListTab()
        {
            this.Driver.FindElement(_wishlistTab).Click();
        }

        public void LoginWithValidCredentials()
        {
            this.Driver.FindElement(_email).SendKeys("test1@yahoo.com");
            this.Driver.FindElement(_password).SendKeys("test1234");
        }
    }
}